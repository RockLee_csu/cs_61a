## CS 61A
#### [Structure and Interpretation of Computer Programs, Spring 2015 version](https://inst.eecs.berkeley.edu/~cs61a/sp15/)

This repository includes all the homeworks, labs, projects I've done when learning CS 61A.

**Lectures**

| lecture No |    Title               | Due date        | Status        |
| :--------: |:----------:            | :---------:     | :-----------: |
|     1      | Functions              | Dec/02/2015 Wed |   Finished    |
|     2      | Names                  | Dec/04/2015 Fri |   Finished    |
|     3      | Control                | Dec/07/2015 Mon |   Finished    |
|     4      | Higher-Order Functions | Dec/09/2015 Wed |   Finished    |
|     5      | Environments           | Dec/11/2015 Fri |   Finished    |
|     6      | Recursion              | Dec/14/2015 Mon |   In-process  |
|     7      | Tree Recursion         | Dec/16/2015 Wed |               |
|     8      | Function Examples      | Dec/18/2015 Fri |               |
|     9      | Data Abstraction       | Dec/23/2015 wed |               |
|     10     | Sequences              | Dec/25/2015 Fri |               |
|     11     | Trees                  | Dec/30/2015 wed |               |
|     12     | Mutable Values         | Jan/01/2016 Fri |               |
|     13     | Mutable Functions      | Jan/04/2016 Mon |               |
|     14     | Objects                | Jan/06/2016 Wed |               |
|     15     | Inheritance            | Jan/08/2016 Fri |               |
|     16     | Representation         | Jan/11/2016 Mon |               |


**labs**

| lab No |    Title                         | Due date        | Status   |
| :----: |:----------:                      | :-----------:   | :------: |
|   0    | Intro to UNIX                    | Dec/02/2015 Wed | Finished |
|   1    | Your own machine                 | Dec/02/2015 Wed | Finished |
|   2    | Functions and Control Structures | Dec/09/2015 Wed | Finished |
|   3    | Recursion                        | Dec/16/2015 Wed |          |
|   4    | Lists                            | Dec/30/2015 wed |          |
|   5    | Recursion                        | Jan/06/2016 Wed |          |

**homeworks**

| homework No | start date      | Due date        | Status        |
| :----:      | :-----------:   | :---------:     | :-----------: |
|   1         | Dec/04/2015 Fri | Dec/09/2015 Wed | Finished      |
|   2         | Dec/11/2015 Fri | Dec/14/2015 Mon | Finished      |
|   3         | Dec/23/2015 wed | Dec/31/2015 Thu |               |
|   4         | Dec/30/2015 wed | Jan/04/2016 Mon |               |
|   5         | Jan/06/2016 Wed |                 |               |

**quizs**

| quiz No | start date      | Due date        | Status        |
| :----:  | :---------:     | :-----------:   | :-----------: |
|   1     | Dec/09/2015 Wed | Dec/10/2015 Thu |  Finished     |

**projects**

| project No |    Title     | start date      | Due date        | Status        |
| :------:   |:----------:  | :---------:     | :-----------:   | :-----------: |
|      1     |     Hog      | Dec/09/2015 Wed | Dec/17/2015 Thu |               |
|      2     |     Maps     | Dec/28/2015 Mon | Jan/07/2016 Thu |               |
|      3     |     Ants     | Jan/08/2016 Fri |                 |               |


**reading**

| chapter No | sub chapter No          |
| :----:     | :---------:             |
|   1        | 1.1, 1.2, 1.3, 1.4, 1.5 |

Last Update: 2015/Dec/30 00:22
